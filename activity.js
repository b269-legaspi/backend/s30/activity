db.fruits.aggregate([
  { $match: { onSale: true } },
  {
    $group: {
      _id: "$supplier_id",
      countFruitsOnSale: {
        $count: {},
      },
    },
  },
]);

db.fruits.aggregate([
  { $match: { stock: { $gte: 20 } } },
  {
    $group: {
      _id: "$supplier_id",
      countFruitsOnSale: {
        $count: {},
      },
    },
  },
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  {
    $group: {
      _id: "$supplier_id",
      avgPriceOfFruitsOnSale: {
        $avg: "$price",
      },
    },
  },
]);

db.fruits.aggregate([
  {
    $group: {
      _id: "$supplier_id",
      maxPriceOfFruits: {
        $max: "$price",
      },
    },
  },
]);

db.fruits.aggregate([
  {
    $group: {
      _id: "$supplier_id",
      minPriceOfFruits: {
        $min: "$price",
      },
    },
  },
]);

/*
1. Create an activity.js file on where to write and save the solution for
the activity.
2. Use the count operator to count the total number of fruits on sale.
3. Use the count operator to count the total number of fruits with
stock more than or equal to 20.
4. Use the average operator to get the average price of fruits onSale
per supplier.
5. Use the max operator to get the highest price of a fruit per
supplier.

Copyright@2019 Tuitt, Inc. and its affiliates. Confidential
Activity

9
6. Use the min operator to get the lowest price of a fruit per supplier.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with
the commit message of Add activity code.
9. Add the link in Boodle.


*/

// Create documents to use for the discussion
db.fruits.insertMany([
  {
    name: "Apple",
    color: "Red",
    stock: 20,
    price: 40,
    supplier_id: 1,
    onSale: true,
    origin: ["Philippines", "US"],
  },

  {
    name: "Banana",
    color: "Yellow",
    stock: 15,
    price: 20,
    supplier_id: 2,
    onSale: true,
    origin: ["Philippines", "Ecuador"],
  },

  {
    name: "Kiwi",
    color: "Green",
    stock: 25,
    price: 50,
    supplier_id: 1,
    onSale: true,
    origin: ["US", "China"],
  },

  {
    name: "Mango",
    color: "Yellow",
    stock: 10,
    price: 120,
    supplier_id: 2,
    onSale: false,
    origin: ["Philippines", "India"],
  },
]);

// Using aggregate method
/*
SYNTAX:
  db.collection.aggregate([
    {$match: {fieldA: valueA} },
    {$group: {_id: "$field", result: {operation}} }
  ]);
*/

db.fruits.aggregate([
  // The "match" is used to pass the documents that meet the specified condition(s) to the next pipeline/aggregation process
  { $match: { onSale: true } },
  // The "$group" is used to group elements together and field value pairs using data from the grouped elements
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
]);

// Field projection with aggregation
/*
SYNTAX: 
  {$project: {field: 1/0}};
*/
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $project: { _id: 0 } },
]);

// Sorting aggregated result
// MINI-ACTIVITY
// 3 minutes
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $sort: { total: -1 } },
]);

// Aggregating results based on array fields
/*
SYNTAX:
  db.collection.aggregate([
    {$unwind: field}
  ]);
*/
db.fruits.aggregate([
  // Deconstructs array field form the input documents to output each element
  { $unwind: "$origin" },
]);

// Displays fruit documents by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
  { $unwind: "$origin" },
  { $group: { _id: "$origin", kind_of_fruits: { $sum: 1 } } },
]);
